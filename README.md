# codestyle

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

> My codestyle rules

My rules for various static code analysis tools.


## Usage

* Use as configuration for [Checkstyle][checkstyle].


### Supported tools

* [Checkstyle][checkstyle] - config based on [Checkstyle configuration][googlechecks] that checks the Google coding 
conventions.
* [FindBugs][findbugs]
* [SpotBugs][spotbugs] - replacing FindBugs
* [CodeNarc][codenarc]
* [PMD][pmd]


### Changes

Changes from the original version:

* Use 4 spaces for indenting instead of 2.
* Extend line length to 120.
* remove [JavadocParagraph](http://checkstyle.sourceforge.net/config_javadoc.html#JavadocParagraph) rule.


## Maintainer

[Markus Reil](https://bitbucket.org/mreil-com/)


[googlechecks]: https://github.com/checkstyle/checkstyle/blob/master/src/main/resources/google_checks.xml "Google Checks"
[checkstyle]: http://checkstyle.sourceforge.net/ "checkstyle"
[findbugs]: http://findbugs.sourceforge.net/ "FindBugs"
[spotbugs]: https://spotbugs.github.io/ "SpotBugs"
[codenarc]: http://codenarc.sourceforge.net/ "CodeNarc"
[pmd]: https://pmd.github.io/ "PMD"
